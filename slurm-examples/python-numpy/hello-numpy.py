import os
import time

import numpy as np
import threadpoolctl

# increase this to get better statistics
NR_REPEATS = 2

print(f"numpy version: {np.__version__}")


a = np.random.random((10_000, 5_000))
b = np.random.random((5_000, 1_000))

# use threadpoolctl.threadpool_info() to find the correct value for 'user_api'
user_api = "blas"

print("Compare performance for 'np.dot(a, b)' with and without multithreading:")
print(f"{a.shape=}, {b.shape=}")

# do fast version first
n = int(os.environ["OMP_NUM_THREADS"])
with threadpoolctl.threadpool_limits(limits=n, user_api=user_api):
    start = time.time()
    for _ in range(NR_REPEATS):
        c = np.dot(a, b)
    end = time.time()
    parallel_time = (end - start) / NR_REPEATS
print(f"took {parallel_time:.2f}s on {n} threads (mean of {NR_REPEATS} repetitions)")

with threadpoolctl.threadpool_limits(limits=1, user_api=user_api):
    start = time.time()
    for _ in range(NR_REPEATS):
        c = np.dot(a, b)
    end = time.time()
    serial_time = (end - start) / NR_REPEATS
print(f"took {serial_time:.2f}s on 1 thread (mean of {NR_REPEATS} repetitions)")


print(f"speedup: {serial_time / parallel_time:.2f}")
