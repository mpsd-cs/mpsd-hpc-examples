mpsd-modules 25a

module load miniforge3/24.11.2-1
source activate python-3.12

python -m venv venv

source venv/bin/activate

pip install numpy threadpoolctl
