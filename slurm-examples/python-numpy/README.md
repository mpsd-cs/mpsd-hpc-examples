# Python-Serial example

This example demonstrates how to run a Python application that makes use of the NumPy module in the cluster.

The source code for the application can be found in `hello-numpy.py`.

You can use this as a starting point to understand how to properly set up and load Python virtual environments for your own applications.

## Instructions

> All instructions assume you are in the directory of this file.

1. `. ./setup-env.sh`

    This script will setup and activate the python virtual environment the example will run inside of.

2. `sbatch ./submission-script.sh`

    This command will submit your job to the cluster with the parameters specified in `submission-script.sh`. As your job is run, your output will be logged to `out.<JOB_ID>` and any errors to `err.<JOB_ID>` where `JOB_ID` is the job number on the slurm queue (given to you when you submit it).
