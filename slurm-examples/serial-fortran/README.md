# Serial-Fortran example

This example demonstrates how to build and run a Fortran application in the cluster.

The source code for the application, a simple "Hello World" can be found in `hello.f90`.

## Instructions

> All instructions assume you are in the directory of this file.

1. `. ./setup-env.sh`

    This script will load the tools you will need to compile the example.

2. `make`

    This command will build the application to be run, according to the instructions in the `Makefile`. For this example, it is fine to build on the login node itself but for larger applications consider creating a job (or requesting an allocation) to build your software in a proper compute node.

    Once this is run, an application named `hello` will be in this directory.

3. `sbatch ./submission-script.sh`

    This command will submit your job to the cluster with the parameters specified in `submission-script.sh`. As your job is run, your output will be logged to `out.<JOB_ID>` and any errors to `err.<JOB_ID>` where `JOB_ID` is the job number on the slurm queue (given to you when you submit it).
