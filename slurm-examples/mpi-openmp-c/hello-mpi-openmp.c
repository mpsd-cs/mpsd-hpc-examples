#include <mpi.h>
#include <omp.h>
#include <stdio.h>

int main(int argc, char** argv) {
  MPI_Init(NULL, NULL);

  int world_size;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  char processor_name[MPI_MAX_PROCESSOR_NAME];
  int name_len;
  MPI_Get_processor_name(processor_name, &name_len);

  #pragma omp parallel
  {
    int thread_idx = omp_get_thread_num();
    int num_threads = omp_get_num_threads();
    printf("Hello world from thread %d of %d on rank %d out of %d on %s.\n",
           thread_idx, num_threads, world_rank, world_size, processor_name);
  }
  MPI_Finalize();
}
