# CUDA example

This example demonstrates how to build and run a CUDA application in the cluster.

> In order to ensure maximum performance, the CUDA module is only available on our GPU nodes. As such, you need to build your software on an actual node with a GPU. This is to ensure matching microarchitecture between the build and run environments.

> For your own work, consider always setting up a job to build your software in the same partition you intend it to run on in order to ensure that microarchitecture matching.

The `hello-cuda.cpp` program is derived from the
[deviceQuery](https://github.com/NVIDIA/cuda-samples/tree/2b689228b746248b7bb7d2778d5ea0701669f7d3/Samples/1_Utilities/deviceQuery)
example provided by Nvidia as a way to test the CUDA toolkit.

> The source files for the application are from external examples provided by nVidia. They are a bit long and the support files a bit broader than what is used in this one example, as they're meant to be shared between multiple of their code samples. So for a basic CUDA code example to study, they may not be the best option.

The program is designed to display the properties of all the CUDA devices
installed on a system. It does this by accessing the GPU cores present on the
system and trying to communicate between them via MPI (CUDA aware MPI).

The purpose of this program is to provide an easy way for users to test and
verify that their CUDA toolkit is installed correctly and that their GPU devices
are functioning as expected. By running hello-cuda program, users can check the
properties of their CUDA devices, such as the number of cores, memory size, and
clock speed, to ensure that they meet their requirements.


## Instructions

> All instructions assume you are in the directory of this file.

### Building the Application

Choose one of the following options:

#### Option A: Building manually on an interactive session

> This workflow would be preferred if you are tweaking your build or fiddling with options.

1. `salloc -t 20 --gpus 1 --cpus-per-task 2 -p gpu`

    This command will request an interactive session on a GPU node. The time limit is 20 minutes, which should be more than enough for this example.

> Once salloc grants you the resource, you will be automatically logged into the gpu node, as indicated by your command prompt, but you will still be working in the same directory.

2. `. ./setup-env.sh`

    This script will load the tools you will need to compile the example.

3. `make`

    This command will build the application to be run, according to the instructions in the `Makefile`. 

    Once this is run, an application named `hello-cuda` will be in this directory. Since you are in a GPU node with CUDA loaded, can try running it manually if you want but this will not work once you're back in the login node.

4. `exit`

    This command will end your session. Slurm, the task scheduler, will terminate your allocation, freeing the resources again. After this command you will be back in the login node, as indicated by your command prompt, and will be ready to submit your job.

#### Option B: Using a slurm job to build your software

> If you just have a bit of software you need to build, this is the preferred way.

1. `sbatch ./submission-build-script.sh`

    This command will submit your job to the cluster with the parameters specified in `submission-build-script.sh`. This job builds the software for the example. As your job is run, your output will be logged to `build-out.<JOB_ID>` and any errors to `build-err.<JOB_ID>` where `JOB_ID` is the job number on the slurm queue (given to you when you submit it). You will also have the `hello-cuda` application in this directory. Just remember that you can't run it from the login node.


### Running the application

1. `sbatch ./submission-script.sh`

    This command will submit your job to the cluster with the parameters specified in `submission-script.sh`. As your job is run, your output will be logged to `out.<JOB_ID>` and any errors to `err.<JOB_ID>` where `JOB_ID` is the job number on the slurm queue (given to you when you submit it).

