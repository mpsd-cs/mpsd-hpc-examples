#include <omp.h>
#include <stdio.h>

int main() {
  #pragma omp parallel
  {
    int thread_idx = omp_get_thread_num();
    int num_threads = omp_get_num_threads();
    printf("Hello from thread %d of %d.\n",
           thread_idx, num_threads);
  }
}
