# MPI-C example

This example demonstrates how to build and run a simple C application across multiple nodes through MPI.

The source code for the application, a simple distributed "Hello World!" can be found in `hello-mpi-openmp.c`.

Under the hood, MPI is also getting the information from the parameters requested in the script (numer of nodes, cpus per task...) in order to know how much resources it has available.

## Instructions

> All instructions assume you are in the directory of this file.

1. `. ./setup-env.sh`

    This script will load the tools you will need to compile the example.

2. `make`

    This command will build the application to be run, according to the instructions in the `Makefile`. For this example, it is fine to build on the login node itself but for larger applications consider creating a job (or requesting an allocation) to build your software in a proper compute node.

    Once this is run, an application named `hello-mpi` will be in this directory.

3. `sbatch ./submission-script.sh`

    This command will submit your job to the cluster with the parameters specified in `submission-script.sh`. As your job is run, your output will be logged to `out.<JOB_ID>` and any errors to `err.<JOB_ID>` where `JOB_ID` is the job number on the slurm queue (given to you when you submit it).

    This script also makes sure to request the `public` partition instead of our default `public2`. Prefer this partition for MPI enabled jobs. You can refer to [our documentation](https://computational-science.mpsd.mpg.de/docs/mpsd-hpc.html#partitions) for details.
