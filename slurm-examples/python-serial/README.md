# Python-Serial example

This example demonstrates a basic python application being run on the cluster.

The source code for the application, a simple "Hello World" can be found in `hello.py`.

You can use this as a starting point to understand how to properly set up and load python for your own tasks.

## Instructions

> All instructions assume you are in the directory of this file.

1. `sbatch ./submission-script.sh`

    This command will submit your job to the cluster with the parameters specified in `submission-script.sh`. As your job is run, your output will be logged to `out.<JOB_ID>` and any errors to `err.<JOB_ID>` where `JOB_ID` is the job number on the slurm queue (given to you when you submit it).
