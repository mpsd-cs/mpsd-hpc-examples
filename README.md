# Example programs and submission scripts for the MPSD HPC system

Instructions on building and running each example can be seen in each of their individual README files.


## Python

These examples show the basics of running Python applications in the cluster.

### python-serial

[This example](slurm-examples/python-serial/) demonstrates how to correctly load Python within the MPSD systems.

### python-numpy

[This example](slurm-examples/python-numpy/) introduces virtual environments and how to set and load them in a reproducible way.

## C and Fortran

These examples show the basics of compiling software to run on the cluster.

### serial-fortran

A simple [Fortran "Hello World"](slurm-examples/serial-fortran/)

### openmp-c

Using C, [this example](slurm-examples/openmp-c/) shows how to build applications leveraging OpenMP for parallelisation and how to set up OpenMP to correctly account for resources made available to a job by Slurm, the task scheduler on the cluster.

### mpi-c

Again on C, [this example](slurm-examples/mpi-c/) shows the basic structure of an MPI application, designed to run across several tasks in the cluster.

### mpi-openmp-c

Finally [this example](slurm-examples/mpi-openmp-c/) demonstrates that both OpenMP and MPI can be combined to correctly spread a job across multiple tasks and across mutliple threads in each task.

## GPU

### CUDA

[This example](slurm-examples/cuda/) demonstrates how to build an run CUDA applications to make use of the GPU compute resources in the cluster.
